# My Successfully Created Projects:

*   [callenq.com/freeminutes](https://callenq.com/freeminutes/) - funnel page
*   [ibs2000.ru](http://ibs2000.ru) - creating new pages and small changes 
*   [yalta-film.ru](http://yalta-film.ru/) - psd to wordpress website
*   [emsfood.by](http://emsfood.by/) - psd to html5/css3/js website
*   [apeshekhonov.webworkers.pro](http://apeshekhonov.webworkers.pro/) - psd to html5/css3/js
*   [denisgrotsky.ru](http://denisgrotsky.ru/) - psd to html5/css3/js
*   [shangshunginstitute.webworkers.pro](http://shangshunginstitute.webworkers.pro/) - psd to wordpress website
*   [inmedex.ru](http://inmedex.ru/) - js and html/css changes
*   [creditservise.ru](http://creditservise.ru/) - html5/css3/js changes, responsive template
*   [solncesvet.ru](http://solncesvet.ru/) - html5/css3/js changes, responsive design
*   [animalavto.ru](http://animalavto.ru/) - responsive design with html/css/js changes
*   [primebeli.by](http://primebeli.by/) - responsive design
*   [kbo.center](http://kbo.center/) - responsive template
*   [top-baby.com.ua](http://top-baby.com.ua/) - responsive template
*   [ktm-katum.ru](http://ktm-katum.ru/) - website from scratch
*   [svezdaprod.webworkers.pro](http://svezdaprod.webworkers.pro) - html5/css3/js changes, responsive template